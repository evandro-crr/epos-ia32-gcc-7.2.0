#/bin/sh
# GCC Toolchain for EPOS on x86 (ia32)

GCC=7.2.0


TARGET=i686-elf
PROG_PREFIX=epos-ia32-
USERNAME=$1

BASE=$PWD
PREFIX=$BASE/epos-gcc-$GCC
EPOS=$BASE/00-base
FILE=$BASE/lixo


# clone svn teaching branch
if [ ! -e $EPOS ] ; then
    svn checkout https://epos.lisha.ufsc.br/svn/teaching/00-base --username $USERNAME
    cd $EPOS
#   make APPLICATION=philosophers_dinner -j5 nao funiona... motivos
    make APPLICATION=philosophers_dinner
fi

# creat file test
if [ ! -e $FILE ] ; then
    echo -e "int main(int argc, char **argv)  {  return argc;  }" > $FILE.cc
fi

$PREFIX/bin/$PROG_PREFIX""g++ -m32 -O2 -fno-rtti -fno-exceptions --no-use-cxa-atexit -c $FILE.cc

$PREFIX/bin/$PROG_PREFIX""ld -m elf_i386 --gc-sections  -L$EPOS/lib -L$PREFIX -L$PREFIX/lib/gcc/$TARGET/$GCC/ -static --nmagic --section-start .init=0x00000000 --section-start .ctors=0x00400000 -o $FILE $EPOS/lib/ia32_crt0.o $EPOS/lib/ia32_crtbegin.o $EPOS/lib/pc_init_first.o $EPOS/lib/pc_init_application.o $EPOS/lib/pc_application.o $FILE.o $EPOS/lib/pc_init_system.o $EPOS/lib/pc_system.o $EPOS/lib/ia32_crtend.o -lutil_ia32 -linit_ia32 -lsys_ia32 -lmach_ia32 -larch_ia32 -lutil_ia32 -lgcc

$PREFIX/bin/$PROG_PREFIX""nm -C $FILE |grep -v EPOS

