#/bin/sh
# GCC Toolchain for EPOS on x86 (ia32)

BINUTILS=2.29.1
GCC=7.2.0
NEWLIB=2.5.0
GMP=6.1.0
MPFR=3.1.4


BASE=$PWD

TARGET=i686-elf
PROG_PREFIX=epos-ia32-

PREFIX=$BASE/epos-gcc-$GCC

BUILD=$BASE/build
CONTRIB=$BASE/contrib

mkdir -p $BUILD $CONTRIB $PREFIX

# Wget
cd $CONTRIB
if [ ! -e binutils-$BINUTILS.tar.bz2 ] ; then
    wget ftp://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS.tar.bz2
fi
if [ ! -e gcc-$GCC.tar.gz ] ; then
    wget ftp://ftp.gnu.org/gnu/gcc/gcc-$GCC/gcc-$GCC.tar.gz
fi
if [ ! -e newlib-$NEWLIB.tar.gz ] ; then
    wget ftp://sources.redhat.com/pub/newlib/newlib-$NEWLIB.tar.gz
fi
if [ ! -e gmp-$GMP.tar.bz2 ] ; then
    wget ftp://gcc.gnu.org/pub/gcc/infrastructure/gmp-$GMP.tar.bz2
fi
if [ ! -e mpfr-$MPFR.tar.bz2 ] ; then
    wget ftp://gcc.gnu.org/pub/gcc/infrastructure/mpfr-$MPFR.tar.bz2
fi

# Unpack
cd $BUILD
tar jxf $CONTRIB/binutils-$BINUTILS.tar.bz2
tar zxf $CONTRIB/gcc-$GCC.tar.gz
tar zxf $CONTRIB/newlib-$NEWLIB.tar.gz
cd gcc-$GCC
tar jxf $CONTRIB/gmp-$GMP.tar.bz2
ln -s gmp-$GMP gmp
tar jxf $CONTRIB/mpfr-$MPFR.tar.bz2
ln -s mpfr-$MPFR mpfr

# Binutils
cd $BUILD
mkdir $TARGET-binutils-build
cd $TARGET-binutils-build
../binutils-$BINUTILS/configure --target=$TARGET --prefix=$PREFIX --program-prefix=$PROG_PREFIX
make -j5
make install

# Newlib
cd $BUILD
mkdir $TARGET-newlib-build
cd $TARGET-newlib-build
../newlib-$NEWLIB/configure --target=$TARGET --prefix=$PREFIX --program-prefix=$PROG_PREFIX
make -j5
make install

# GCC (C and C++)
cd $BUILD
mkdir $TARGET-gcc-build
cd $TARGET-gcc-build
../gcc-$GCC/configure --target=$TARGET --prefix=$PREFIX --program-prefix=$PROG_PREFIX --enable-languages=c,c++ --disable-nls --disable-multilib --disable-libssp --with-newlib --disable-shared --disable-threads --enable-target-optspace --with-gnu-as --with-gnu-ld --without-headers --disable-initfini-array
make -j5
make install

